package Constant;

/**
 * Created by anle on 27/12/2015.
 */
public enum FragmentID {
    Game("Game"),
    TinTuc("TinTuc"),
    Home("home"),
    Main("main");
    private String key;
    public String getKey()
    {
        return key;
    }
    private FragmentID(String key)
    {
        this.key=key;
    }
}
