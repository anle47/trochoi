package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.bai1.R;

/**
 * Created by anle on 08/01/2016.
 */
public class GameAdapter extends BaseAdapter {
    String[] data=new String[]{
        "Truc Xanh","Xephinh"
    };
    private Context context;
    public GameAdapter(Context context){
        this.context=context;
    }
    @Override
    public int getCount() {
            return data.length;
    }

    @Override
    public Object getItem(int position) {
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=LayoutInflater.from(context);
        convertView=inflater.inflate(R.layout.game_item_main, parent, false);
        TextView btngame= (TextView) convertView.findViewById(R.id.btngame);
        btngame.setText(getItem(position).toString());
        btngame.setVisibility(convertView.VISIBLE);
        return convertView;
    }
}
