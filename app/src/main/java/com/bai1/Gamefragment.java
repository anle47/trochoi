package com.bai1;

import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;

import Constant.FragmentID;
import adapter.GameAdapter;

/**
 * Created by anle on 08/01/2016.
 */
public class Gamefragment extends Basefragment implements AdapterView.OnItemClickListener {
    ListView listView;
    @Override
    public int getid() {
        return R.layout.game_fragment;
    }

    @Override
    public void inView(View view) {
        listView= (ListView) view.findViewById(R.id.list);
        GameAdapter gameAdapter=new GameAdapter(getActivity());
        listView.setAdapter(gameAdapter);
        listView.setOnItemClickListener(this);


    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MainActivity mainActivity= (MainActivity) getActivity();
        switch (position)
        {
            case 0:
                System.out.println(position);
                mainActivity.addFragment(FragmentID.Main,null,true);
                break;
            case 1:
                mainActivity.addFragment(FragmentID.Main,null,true);
        }
    }
}
