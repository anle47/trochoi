package com.bai1;

import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

/**
 * Created by anle on 27/12/2015.
 */
public class MainFragment extends Basefragment {
    @Override
    public int getid() {
        return R.layout.mainfragment;
    }

    @Override
    public void inView(View view) {
        RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.hinh);
        int dong=0;
        for (int i = 0; i < 6; i++) {
            Button btn = new Button(this.getActivity());

            if (i == 0) {
                btn.setText("" + i);
                btn.setId(i+1);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                btn.setLayoutParams(params);
                dong=1;
//                relativeLayout.addView(btn);
            }
            else
            if(i%3==0) {
                btn.setText("" + i);
                btn.setId(i+1);
                RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.BELOW,dong);
                btn.setLayoutParams(params);
                dong+=3;
            }
            else {
                btn.setText("" + i);
                btn.setId(i + 1);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.RIGHT_OF, i);
                params.addRule(RelativeLayout.BELOW,dong-3);
                btn.setLayoutParams(params);
//                relativeLayout.addView(btn);


            }
                relativeLayout.addView(btn);
            }

        }
//    }
}
