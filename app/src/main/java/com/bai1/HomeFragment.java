package com.bai1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import Constant.FragmentID;

/**
 * Created by anle on 27/12/2015.
 */
public class HomeFragment extends Basefragment {
    @Override
    public int getid() {
        return R.layout.homefragment;
    }

    @Override
    public void inView(View view) {
        Button star= (Button) view.findViewById(R.id.btnbatdau);
        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity) getActivity();
                       activity .addFragment(FragmentID.Game, null, true);

            }
        });

    }
}
