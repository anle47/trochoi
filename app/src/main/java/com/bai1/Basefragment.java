package com.bai1;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by anle on 27/12/2015.
 */
public abstract class Basefragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int id=getid();
        View view=inflater.inflate(id,null,false);
        inView(view);
        return view;
    }
    public abstract int getid();
    public abstract void inView(View view);
}
